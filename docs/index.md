# Welcome to Oli's Standard Libraries Documentation

A modern C++ header only library full of useful code snippets. All
headers mimic the C++ standard library conventions, and must be
included as `#include <olistd/thing>` where `thing` is a particular
part of the library. 

Code hosted on [gitlab](https://gitlab.com/OFThomas/olistd)!

## Install

As this library is header only you can simply copy the folder
`include/olistd` into your code.

You can just run 
```sh 
git clone https://gitlab.com/OFThomas/olistd.git
``` 
inside your project and you'll need to include `./olistd/include` into your build
process.

### To install system wide 
This copies the header files into `/usr/local/include` by default.

```sh
git clone https://gitlab.com/OFThomas/olistd.git
cd olistd && cmake . && sudo cmake --install .
```
## Uninstall 

To remove, all you need to do is delete the headers 
```
sudo rm -rf /usr/local/include/olistd
```

## Requirements 

**None** 

Optional, in order to build the documentation locally you will need 
```
pip install mkdocs pymdown-extensions
```

See the [README](https://gitlab.com/OFThomas/olistd/-/blob/main/README.md)!

# Quick start / Conventions 

We'll go through the main features alphabetically as that's easier
for me to remember where things are.

**Throughout this library the default template type is `T`**

# Combinatorics 
The [combinatorics](combinatorics.md) library includes:

* Combinations
* Powersets
* Set partitions
* Counting in arbitrary bases
* Click patterns

# Derivatives

The [derivative](derivatives.md) library includes finite difference methods for:

* Any order (recursive) single parameter functions double(double)
* Any order mixed partial derivatives for function with the signature
  double(const std::vector<double> &)


## Cached function calls

There's an experimental class which you can use to cache the results of
expensive function calls, repeated calls to the function with the same arguments
will use the saved value.

```c
template <typename F>
olistd::working::Cached_funtion(cont F & func);
```
Usage is as follows 
```c
auto my_func = [](double t, float y, int b) // some args 
{
    // imagine some complex function that takes a while to evaluate
    return t * y + std::pow(b, b);
}

olistd::working::Cached_function cached_f{my_func};

// then usage is equivalent to my_func directly 
cached_f(1.0, 0.5f, 7) == my_func(1.0, 0.5f, 7);
```

# Flat_map

The [flat_map](flat_map.md) library includes an implementation for an
associative container for [key, value] pairs using two `std::vector`s.

# Graph_draw
[graph_draw](graph_draw.md)

# IO

The [io](io.md) library contains functions for printing STL containers to stdout
(the terminal) and writing to files (disk)

# Maths

The [maths](maths.md) library contains 

# Optimizer

The [optimizer](optimizer.md) library contains classes and functions for
numerical optimization:

* Direct search methods
* Gradient based (finite difference) methods


# Packed_int

The [packed_int](packed_int.md) library contains the Packed_int class.

# Random

The [random](random.md) library contains a bunch of helper functions for quickly
generating random numbers using C++ methods. 

# String

The [string](string.md) 

# Terminal_draw
[terminal_draw](terminal_draw.md)

# Terminal_plot
[terminal_plot](terminal_plot.md)

# Threads
[threads](threads.md)

# Timer
[timer](timer.md)


# Vector
[vector](vector.md)


