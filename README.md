# Olis standard lib

A header only library featuring a bunch of useful functions for
timing, printing and combinatorics I've seen around for C++.

Simply include the header where needed. Requires C++11 or higher. 

# SEE THE DOCS 
[https://ofthomas.gitlab.io/olistd/](https://ofthomas.gitlab.io/olistd/)

## To Install mkdir build && cd build && cmake ..

sudo cmake --install .

## To remove sudo rm -r /usr/local/include/olistd

## Apache License Version 2.0, 

Copyright (c) [2022] [Oliver Thomas]

