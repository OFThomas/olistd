/**
 * @class       : mathjax
 * @author      : oli (oli@oli-HP)
 * @created     : Friday Nov 18, 2022 22:58:53 GMT
 * @description : mathjax
 */

window.MathJax = {
  tex: {
    inlineMath: [["\\(", "\\)"]],
    displayMath: [["\\[", "\\]"]],
    processEscapes: true,
    processEnvironments: true
      tags: 'ams'
  },
  options: {
    ignoreHtmlClass: ".*|",
    processHtmlClass: "arithmatex"
  }
};

document$.subscribe(() => { 
  MathJax.typesetPromise()
})

