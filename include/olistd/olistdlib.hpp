/** @file olis_std_lib @author O Thomas @date 2019 @brief Oli's std c++ library.
 * Uses Eigen for matrices, writing to files and reading in csv. Including a few
 * math functions.
 *
 * Copyright 2022 Oliver Thomas
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 * Code from online sources has been referenced where possible, if you think
 * I've missed anything please write to me and I will update.
 */

#ifndef olistdlib_hpp
#define olistdlib_hpp

#include <olistd/version>

#include <olistd/combinatorics>
#include <olistd/derivatives>
#include <olistd/flat_map>

#include <olistd/graph_draw>
#include <olistd/io>
#include <olistd/maths>

#include <olistd/optimizer>
#include <olistd/packed_int>
#include <olistd/random>

#include <olistd/string>
#include <olistd/terminal_draw>
#include <olistd/terminal_plot>

#include <olistd/timer>
#include <olistd/vector>

namespace olistd
{


} // end of olistd namespace

#endif // olistdlib_hpp
