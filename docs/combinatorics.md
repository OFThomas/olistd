# Combinatorics

Used by:
```c
#include <olistd/combinatorics>
```

Nearly all of the functions in this file are used to generate
permutations and return corresponding `std::vector<T>` or nested
vectors, `std::vector<std::vector<T>>`.

## Combinations

A combination referes to a subset of elements from a set of
numbers. Consider the set of numbers $v = \{0,1,2\}$ there are $3$
choices of picking exactly one number out of our set of values $v$. 

$$
s_0 = \{0\}, s_1 = \{1\}, s_2 = \{2\} \, .
$$

The notation for the number of ways to pick $r$ numbers from a total
of $n$ numbers is $n \choose r$, which is read as "n choose r".

```
std::vector<int> olistd::lexi_combination(int n, int p, int x);
```
A memory efficient function for iterating through combinations. 
Typical usage is if you want to iterate through all $n \choose p$
combinations, would be:

```
int n = 3;
int p = 2;
int num = olistd::binomial_coeff(n, p); // 3 choose 2 = 3
for(int x = 0; x < num; x++)
{
    std::vector<int> combination = olistd::lexi_combination(n, p, x);
    olistd::print(combination);
}
```
Which would print:
```
(0, 1) // x = 0
(0, 2) // x = 1
(1, 2) // x = 2
```

For small cases you can instead generate a `std::vector` of all of
the combinations (rather than iterate through sequentially):

```
template <typename T>
std::vector<std::vector<T>> olistd::nchooser(const std::vector<T> & n, int r);
```
note that this function is templated so works for any vector of
value types e.g. `float`, `double`, `int` etc. There is an
additional function overload of 

```
std::vector<std::vector<int>> olistd::nchooser(int n, int r);
```
which takes an integer n and generates the `std::vector` of size
`n` with values `{0, 1, ..., n-1}`.

For example:
```c
std::vector<std::vector<int>> combinations = olistd::nchooser(3, 2);

combinations[0] == {0, 1};
combinations[1] == {0, 2};
combinations[2] == {1, 2};
```
which is the same as the `olistd::lexi_combination` case above.


## Powersets

A powerset is all of the combinations of choosing all sized subsets
of a list of numbers, basically just generate all of the different
sized combinations of a set and append them all together to generate
the powerset of that set.  

Consider the set of numbers $v = \{0, 1, 2\}$, the powerset of $v$
is:

$$
\begin{align}
2^v = \{ & \\
        &\{\}, \\
        &\{0\}, \{1\}, \{2\}, \\
        &\{0, 1\}, \{0, 2\}, \{1. 2\}, \\
        &\{0, 1, 2\} \\
 \}&
\end{align}
$$

when written as above we can see that each line corresponds to the
set of combinations of $v \choose r$, for line $r$.

```
template <typename T>
std::vector<T> olistd::lexi_powerset(const std::vector<T> & vect,
        std::uint64_t j);
```
Similar to `olistd::lexi_combination`, `olistd::lexi_powerset<T>`
can be used to iterate through a powerset.
```
std::vector<int> v = {0, 1, 2};
int num = std::pow(2, v.size()); // 2^3 = 8
for(int x = 0; x < num; x++)
{
    std::vector<int> ps = olistd::lexi_powerset(v, x);
    olistd::print(ps);
}
```
Which would print as the equation above for $2^v$:
```
() // x = 0 // empty vector 
(0) // x = 1
...
(0, 1, 2) // x = 7
```

There's an overload for `int` again called
``` 
std::vector<int> olistd::lexi_powerset_idx(const std::vector<T> &
vect, int j);
```

And just like the `olistd::combination` function there is a powerset
function for generating all of the sets in the powerset in one go,
it uses a nested vector, `std::vector<std::vector<T>>`
```
template <typename T>
std::vector<std::vector<T>> olistd::powerset(const std::vector<T> & vect);
```

## Set partitions

Partitions of a set are kind of similar to the power set but with an important
distinction. 

Consider (our favourite) set of numbers $v = \{0, 1, 2\}$, there are 5
partitions of this set (the number of partitions of a set with $n$ element is
given by the Bell number $B_n$):

$$
\begin{align}
\{ \\
&\{ \{0\}, \{1\}, \{2\} \} \\ 
&\{ \{0, 1\}, \{2\} \} \\
&\{ \{0, 2\}, \{1\} \} \\
&\{ \{0\}, \{1, 2\} \} \\
&\{ \{0, 1, 2\} \} \\
\}
\end{align}
$$

``` 
template <typename T>
std::vector<std::vector<std::vector<T>>> olistd::set_partitions(
    const std::vector<T> & vect);
```
Where each level of curly brace corresponds to one of the nested `std::vector`
in our function. For example:
```
std::vector<int> v{0, 1, 2};
std::vector<std::vector<std::vector<int>>> part = olistd::set_partitions(v);

std::vector<std::vector<int>> p0 = part[0];
p0 == {{0}, {1}, {2}};
std::vector<int> group0 = p0[0]; // == {0}
std::vector<int> group1 = p0[1]; // == {1}
std::vector<int> group2 = p0[2]; // == {2}

// and the last element is
part.back() // == { {0, 1, 2} };
```

## Counting in arbitrary bases

Given a `std::vector<int> vect` of values in base `n` this **takes
`vect` by reference** and adds `1` to the vector in base `n`:
```
void olistd::count_base_n(std::vector<int> & vect, int n = 2);
```
For example:
```c
std::vector<int> v = {0, 0};
const int base = 2;

olistd::count_base_n(v, 2);
olistd::print(v); // v == {0, 1};

olistd::count_base_n(v, 2);
olistd::print(v); // v == {1, 0};

olistd::count_base_n(v); // the default base is 2 (binary)
olistd::print(v); // v == {1, 1};
```

For Gray codes.
```
unsigned olistd::binary_to_Gray(unsigned num);
```

```
void olistd::toGray(unsigned base, unsigned digits, unsigned value,
        std::vector<unsigned> & gray);
```


## Click patterns

Similar to generating and iterating through combinations, there are
some useful functions for generating _click patterns_. A click
pattern is a `std::vector<int>` of values corresponding to either
sets of zeros and ones, possibly with a set number of ones in total,
or just a `std::vector<int>` where we want the sum of the values to
add to a specific value.

```
std::vector<std::vector<int>> olistd::click_patterns(int modes, 
        int max_photon, int total_photons = -1, bool equal = false);
```
We return a nested `std::vector` where the outer index is over click
patterns and the inner `std::vector<int>` corresponds to one
particular click pattern.  

For the arguments to the function, `modes` corresponds the size of the click
pattern, i.e. `modes = 2` will generate vectors of size two. `max_photon` sets
the maximum value for each element of the output vectors, basically this the
`base` of the number which we're counting up in from `olistd::count_base_n`
above. There are two default args `total_photons` which sets the a maximum sum
of each click pattern generated, this is useful if you want to generate all of
the sub click patterns which don't exceed a total value. A value of
`total_photons = -1` means allow all click patterns (the default case). Lastly
the flag `equal = false` will only generate the click patterns which sum exactly
to the value of `total_photons`.  

Example usage might be as follows:
```
const int m = 3;
const int base = 2;

std::vector<std::vector<int>> clicks = olistd::click_patterns(m, base);

clicks[0] == {0, 0, 0};
clicks[1] == {0, 0, 1};
clicks[2] == {0, 1, 0};
clicks[3] == {0, 1, 1};
// ...
clicks[7] == {1, 1, 1};

```

**Most of the time you will not actually want to use `olistd::click_patterns`
but instead some of the helper functions below**.


#### Photon number resolved subspace click patterns
```
std::vector<std::vector<int>> olistd::gen_pnr_click_patterns_k_subspace(
        size_t num_modes, int max_photons, int total_photons);
```
Returns clicks patterns where the maximal element is `max_photons` and we only
return the click patterns which have exactly `total_photons` in them, for
example:
```c
const int m = 3;
const int max = 2;
const int total = 2;

std::vector<std::vector<int>> clicks = olistd::gen_pnr_click_patterns_k_subspace(m, max, total);

clicks[0] == {0, 0, 2};
clicks[1] == {0, 1, 1};
clicks[2] == {0, 2, 0};
clicks[3] == {1, 0, 1};
clicks[4] == {1, 1, 0};
clicks[5] == {2, 0, 0};
```

#### Threshold detector click patterns

```
std::vector<std::vector<int>> olistd::gen_threshold_click_patterns(
        size_t num_modes);
```
Returns all the 0,1 bitstrings of size `num_modes`:
```
std::vector<std::vector<int>> clicks = olistd::gen_threshold_click_patterns(3);

clicks[0] == {0, 0, 0};
clicks[1] == {0, 0, 1};
// ...
clicks[7] == {1, 1, 1};
```

#### Threshold subspace click patterns
```
std::vector<std::vector<int>> olistd::k_click_patterns(int n, int k);
```
Returns the 0,1 bitstrings with exactly `k` ones in them:
```c
std::vector<std::vector<int>> clicks = olistd::k_click_patterns(3, 2);

clicks[0] == {1, 1, 0};
clicks[1] == {1, 0, 1};
clicks[2] == {0, 1, 1};
```
This function is similar to the combination functions above.

#### Click patterns between k_min and k_max click subspaces 
```
std::vector<std::vector<int>> olistd::gen_click_patterns(
        int graph_size, int k_max, int k_min);
```
```
std::vector<std::vector<int>> olistd::gen_click_patterns(
        int graph_size, int k_max);
```
These functions return a collection of click patterns which span multiple click
subspaces, the same result is achieved by concatonating the outputs of
`olistd::k_click_patterns` for a range of `k` values:

```c
const int m = 3;
const int max = 2;
const int min = 1;
std::vector<std::vector<int>> clicks = olistd::gen_click_patterns(m, max, min);

clicks[0] == {1, 0, 0};
clicks[1] == {0, 1, 0};
clicks[2] == {0, 0, 1};
clicks[3] == {1, 1, 0};
clicks[4] == {1, 0, 1};
clicks[5] == {0, 1, 1};
```


