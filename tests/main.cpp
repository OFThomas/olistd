/**
 * @author      : oliver (oliver@oliver-pc)
 * @file        : main
 * @created     : Wednesday May 18, 2022 21:57:52 BST
 */

#include <iostream>
#include "olistd/olistdlib.hpp"

#include <olistd/threads>

int switch_stuff(int choice);

int main(int argc, char * argv[])
{
    std::cout << "Olistd lib tests \n"
        << " Case 0: ALL \n"
        << " Case 1: Flat_map \n"
        << " Case 2: Clicks to packed_int \n"
        << " Case 3: UNIT TEST Clicks to packed_int \n"
        << " Case 4: Packed integer random value tests \n"
        << " Case 5: Packed maps file io for bitstrings \n"
        << " Case 6: Flat map timings for different packed types \n"
        << " Case 7: Terminal plot tests \n"
        << " Case 8: Terminal drawing \n"
        << " Case 9: Terminal drawing polygons \n"
        << " Case 10: Unicode chars \n"
        << " Case 11: Optimizers \n"
        << " Case 12: Graph drawing \n"
        << " Case 13: oli threads \n"
        << " Case 14: working Cached_function (double)(std::vector<double>&) class \n"
        << " Case 15: Variadic Cached_function class \n"
        << std::endl;

    const int max_choices = 8;
    int choice;
    if(argc > 1)
        choice = std::stoi(argv[1]);
    else 
        std::cin >> choice;

    if(choice == 0)
    {
        for(int val = 1; val < max_choices; val++)
        {
            switch_stuff(val);
        }
    }
    else
    {
        switch_stuff(choice);
    }

    return 0;
}

int switch_stuff(int choice)
{
    switch(choice)
    {
        case 1:
            {

                olistd::Flat_map<int, int> map;

                std::vector<int> vals = olistd::lin_vec(0, 10, 10);
                olistd::random_elements_inplace(vals);
                olistd::print(vals);
                for(auto val : vals)
                    map.insert(val, 100 - val);

                for(size_t i = 0; i < map.size(); i++)
                {
                    const auto & key = map.key(i);
                    auto & val = map.value(i);
                    std::cout << "key " << key << ", val " << val << std::endl;
                }

                for(auto key : vals)
                    std::cout << "key " << key << ", lookup " << map.at(key) << std::endl;

                break;
            }

        case 2:
            {
                // for(size_t size : {24, 63, 64, 65, 127, 128, 129})
                for(size_t size : {4, 8, 16, 20})
                {
                    size_t base = 2;
                    std::vector<int> clicks = olistd::random_vector<int>(size, base - 1); 
                    std::cout << "click ";// << std::endl;
                    olistd::print(clicks);

                    olistd::Packed_int<uint8_t> index{clicks, (uint8_t)base};
                    auto ints = index.get_ints();
                    std::cout << "ints " << ints << std::endl;
                    // olistd::print(ints);

                    //std::vector<int> vals = olistd::int_to_vector(ints, base, clicks.size());
                    std::vector<int> vals;
                    std::cout << "vals ";// << std::endl;
                    olistd::print(vals);
                    std::cout << std::endl;

                    std::cout << (clicks == vals) << std::endl;
                    std::cout << std::endl;
                }
                break;
            }
        case 3:
            {
                // unit test
                for(size_t number_of_things = 1; number_of_things < 50; number_of_things++)
                {
                    // int random_base = olistd::random_int(2, 100);
                    // size_t base = random_base;
                    size_t base = 2;
                    std::vector<int> clicks = olistd::random_vector<int>(number_of_things, base - 1); 

                    // olistd::index<uint8_t> index{clicks, base};
                    olistd::Packed_int<uint16_t> index{clicks, (uint8_t)base};

                    auto ints = index.get_ints();

                    // std::vector<int> vals = olistd::int_to_vector(ints, base, clicks.size());
                    std::vector<int> vals;

                    if(clicks != vals)
                    {
                        std::cout << "number of things = " << number_of_things << " base " << base << std::endl;
                        std::cout << "click ";// << std::endl;
                        olistd::print(clicks);
                        std::cout << "ints " << ints << std::endl;
                        // olistd::print(ints);
                        std::cout << "vals ";// << std::endl;
                        olistd::print(vals);

                        std::cout << std::endl;
                        abort();
                    }
                    std::cout << (clicks == vals) << std::endl;
                    std::cout << std::endl;
                }
                break;
            }
        case 4:
            {
                const size_t dim = 32;
                const bool debug = true;
                for(int i = 0; i < 1; i++)
                {
                    size_t base = 2;
                    std::vector<int> v_ints = olistd::random_vector<int>(dim, base-1);

                    using int_type = std::uint64_t;
                    std::cout << "v_ints ";
                    olistd::print(v_ints);

                    std::cout << " ------- PACK ------------" << std::endl;
                    int_type number = olistd::pack_ints_to_num<int>(v_ints, base, debug);
                    std::cout << " number = " << number << std::endl;

                    std::cout << " ------- UN PACK ------------" << std::endl;
                    std::vector<int> reconstruct_v_ints = olistd::unpack_num_to_ints<int>(number, base,v_ints.size(), debug);
                    std::cout << "number = " << number << ", base " << base << std::endl;
                    olistd::print(reconstruct_v_ints);

                    if(v_ints != reconstruct_v_ints)
                    {
                        std::cout << "ERRORORORR NOT THE SAME " << std::endl;
                        abort();
                    }

                }

                break;
            }

        case 5:
            {
                auto clicks = olistd::gen_threshold_click_patterns(24);
                // olistd::print(clicks);

                std::vector<int> rand_nums = olistd::random_vector<int>(clicks.size());

                {
                    olistd::Timer timer;
                    std::map<std::vector<int>, int> basic_map;
                    for(size_t i = 0; i < clicks.size(); i++)
                        basic_map[clicks[i]] = rand_nums[i];

                    olistd::write_file(basic_map, "basicmap_test.txt");
                    timer.stop();
                }
                {
                    olistd::Timer timer;
                    std::map<olistd::Packed_int<std::uint32_t>, int> packed_map;
                    for(size_t i = 0; i < clicks.size(); i++)
                    {
                        packed_map[{clicks[i], 2}] = rand_nums[i];
                    }

                    olistd::write_file(packed_map, "packed_map_inplace_test.txt");
                    timer.stop();
                }

                {
                    olistd::Timer timer;
                    std::map<int, int> packed_map;
                    for(size_t i = 0; i < clicks.size(); i++)
                    {
                        int packed = olistd::pack_ints_to_num<int>(clicks[i], 2);
                        packed_map[packed] = rand_nums[i];
                    }

                    olistd::write_file(packed_map, "packedmap_test.txt");
                    timer.stop();
                }

                break;
            }
        case 6:
            {

                olistd::Flat_map<int, int> map;

                std::vector<int> vals = olistd::lin_vec(0, 10, 10);
                olistd::random_elements_inplace(vals);
                olistd::print(vals);
                for(auto val : vals)
                    map.insert(val, 100 - val);

                for(size_t i = 0; i < map.size(); i++)
                {
                    const auto & key = map.key(i);
                    auto & val = map.value(i);
                    std::cout << "key " << key << ", val " << val << std::endl;
                }

                for(auto key : vals)
                    std::cout << "key " << key << ", lookup " << map.at(key) << std::endl;

                std::vector<std::vector<int>> clicks = olistd::gen_threshold_click_patterns(24);
                // olistd::print(clicks);

                std::vector<int> rand_nums = olistd::random_vector<int>(clicks.size());
                {
                    olistd::Timer timer;
                    std::map<std::vector<int>, int> basic_map;
                    for(size_t i = 0; i < clicks.size(); i++)
                        basic_map[clicks[i]] = rand_nums[i];

                    olistd::write_file(basic_map, "basicmap_test.txt");
                    timer.stop();
                }

                {
                    olistd::Timer timer;
                    olistd::Flat_map<std::vector<int>, int> flat_basic_map;
                    for(size_t i = 0; i < clicks.size(); i++)
                        flat_basic_map[clicks[i]] = rand_nums[i];

                    olistd::write_file(flat_basic_map, "flat_basicmap_test.txt");
                    timer.stop();
                }

                {
                    olistd::Timer timer;
                    std::map<olistd::Packed_int<int>, int> packed_basic_map;
                    for(size_t i = 0; i < clicks.size(); i++)
                        packed_basic_map[olistd::Packed_int<int>{clicks[i]}] = rand_nums[i];

                    olistd::write_file(packed_basic_map, "packed_basicmap_test.txt");
                    timer.stop();
                }

                {
                    olistd::Timer timer;
                    olistd::Flat_map<olistd::Packed_int<int>, int> packed_flat_basic_map;
                    for(size_t i = 0; i < clicks.size(); i++)
                        packed_flat_basic_map[olistd::Packed_int<int>{clicks[i]}] = rand_nums[i];

                    olistd::write_file(packed_flat_basic_map, "packed_flat_basicmap_test.txt");
                    timer.stop();
                }
                break;
            }
        case 7:
            {
                std::vector<int> values = olistd::lin_vec<int>(10);
                olistd::Terminal_plot plot{values};

                plot.draw();


                break;
            }
        case 8:
            {
                olistd::Terminal_draw<int> draw;
                std::vector<std::vector<char>> data;

                const size_t dim = 10;
                std::vector<char> line(dim, '+');
                for(size_t i = 0; i < dim; i++)
                    data.push_back(line);

                bool even = false;
                for(auto & v : data)
                {
                    for(auto & str : v)
                    {
                        if(even)
                            str = '$';
                    }
                    even = !even;
                }
                olistd::print(data);
                draw.draw(data);

                for(auto & v : data)
                {
                    for(auto & str : v)
                    {
                        if(even)
                            str = '$';
                        even = !even;
                    }
                }
                olistd::print(data);
                draw.draw(data);


                break;
            }
        case 9:
            {

                // // make the vector of vectors 
                // size_t rows = 4;
                // size_t cols = 6;
                // // std::vector<std::vector<std::string>> data(rows, std::vector<std::string>(cols));
                // {
                //     std::vector<std::vector<char>> data = olistd::nested_vector<char>(rows, cols);

                //     char a = 33;
                //     for(size_t row = 0; row < rows; row++)
                //     {
                //         for(size_t col = 0; col < cols; col++)
                //         {
                //             // add_point({row,col, a});
                //             data[row][col] = a++;
                //         }
                //     }

                //     olistd::print(data);
                //     Terminal_draw disp;
                //     disp.draw(data);

                //     for( auto point : points)
                //         point.print();
                // }


                olistd::Polygon shape;
                olistd::Point p1{1, 1};
                olistd::Point p2{7, 0};
                olistd::Point p3{9, 5};
                olistd::Point p4{0, 6};
                shape.add_line(p1, p2);
                shape.add_line(p2, p3);
                shape.add_line(p3, p4);
                shape.add_line(p4, p1);

                shape.draw();
                shape.scale(3,2);
                // shape.draw();
                // shape.draw(20, 15);

                olistd::Triangle tri{ {4, 3}, {6, 6}, {10,2}};

                tri.draw();
                // tri.draw(18,8);

                olistd::Polygon shifted{tri};
                shifted.translate(+10, 4);
                shifted.add(tri);
                // shifted.draw();

                shape.add(shifted);

                shape.draw();
                break;
            }
        case 10:
            {
                olistd::Polygon shape;

                // constexpr std::string lslope = '\u2572';
                // char32_t lslope = U"\u2572";
                // wchar_t lslope = "\u2572";
                // std::cout << std::string{lslope} << std::endl;

                const std::string left_slope = "\u2572";
                const std::string right_slope = "\u2571";
                const std::string cross = "\u2573";

                const std::string se_arc = "\u256D";
                const std::string sw_arc = "\u256E";
                const std::string nw_arc = "\u256F";
                const std::string ne_arc = "\u2570";

                const std::string se_arcs = "\u25DC";
                const std::string sw_arcs = "\u25DD";
                const std::string nw_arcs = "\u25DE";
                const std::string ne_arcs = "\u25DF";

                const std::string lhs_bar = "\u23B8";
                const std::string rhs_bar = "\u23B9";

                const std::string h_bar = "\u23BA";
                const std::string hm_bar = "\u23BB";
                const std::string m_bar = "\u2500";
                // const std::string m_bar = "\u23AF";
                const std::string lm_bar = "\u23BC";
                const std::string l_bar = "\u23BD";

                const std::string anticlock = "\u2B6F";
                const std::string clock = "\u2B6E";

                const std::string wuturn = "\u2B8C";
                const std::string nuturn = "\u2B8D";
                const std::string euturn = "\u2B8E";
                const std::string suturn = "\u2B8F";

                const std::string ne_angled = "\u29A6";
                const std::string se_angled = "\u29A7";

                const std::string curly_or = "\u22CE";
                const std::string curly_and = "\u22CF";

                const std::string u_or = "\u22C1";
                const std::string u_and = "\u22C0";

                std::cout << wuturn << nuturn << euturn << suturn << std::endl;
                std::cout << clock << anticlock << std::endl;
                std::cout << h_bar << hm_bar << m_bar << lm_bar << l_bar << std::endl;

                std::cout << " - " << " \\ " << " \u2572 " << " | " << " \u2571 " << " / " << " - " << std::endl;
                std::cout << " _     _ " << std::endl;
                std::cout << "  \\   /  " << std::endl;
                std::cout << "   " << left_slope << " " << right_slope << "  " << std::endl;       
                std::cout << "    |     " << std::endl;

                std::cout << nw_arc << ne_arc << sw_arc << se_arc << std::endl;
                std::cout << se_arc << sw_arc << std::endl;
                std::cout << ne_arc << nw_arc << std::endl;

                std::cout << nw_arcs << ne_arcs << sw_arcs << se_arcs << std::endl;

                std::cout << l_bar << " " << l_bar << std::endl;
                std::cout << " " << cross << " " << std::endl;
                std::cout << h_bar << " " << h_bar << std::endl;

                std::cout << "\u29A7 " << se_angled << std::endl; 
                std::cout << " " << cross << " " << std::endl;
                std::cout << "- " << ne_angled << std::endl; 

                std::cout << m_bar << sw_arc << " " << se_angled << m_bar << std::endl; 
                std::cout << "  " << cross << "  " << std::endl;
                std::cout << m_bar << nw_arc << " " << ne_angled << m_bar << std::endl; 

                std::cout << m_bar << curly_or << " " << se_angled << m_bar << std::endl; 
                std::cout << m_bar << curly_and << "  " << std::endl;

                std::cout << m_bar << u_or << m_bar << std::endl;
                std::cout << m_bar << u_and << m_bar << std::endl;

                std::cout << "\u2AD7" << " " << "\u2AD8" << std::endl;

                std::cout << m_bar << u_or << m_bar << std::endl;
                std::cout << m_bar << "\u2038" << m_bar << std::endl;

                std::cout << m_bar << sw_arcs << se_arcs << m_bar << std::endl;
                std::cout << m_bar << nw_arcs << ne_arcs << m_bar << std::endl;

                std::cout << m_bar << sw_arc << se_arc << m_bar << std::endl;
                std::cout << m_bar << nw_arc << ne_arc << m_bar << std::endl;

                const std::string t_paren = "\u23DC";
                const std::string b_paren = "\u23DD";

                const std::string b_form = "\uFE40";
                const std::string t_form = "\uFE3F";


                const std::string b_turt = "\u23E1";
                const std::string t_turt = "\u23E0";

                const std::string b_turt1 = "\uFE3A";
                const std::string t_turt1 = "\uFE39";

                std::stringstream line1;
                std::stringstream line2;

                const std::string downarr = "\u2304";
                const std::string uparr = "\u2303\u200D";

                for(size_t i = 0; i < 8; i++)
                {
                    line1 << m_bar << downarr << m_bar << " ";
                    line2 << m_bar << uparr << m_bar << " ";
                }

                line1 << m_bar << b_paren << m_bar << " ";
                line2 << m_bar << t_paren << m_bar << " ";

                line1 << m_bar << b_form << m_bar << " ";
                line2 << m_bar << t_form << m_bar << " ";

                line1 << m_bar << b_turt << m_bar << " ";
                line2 << m_bar << t_turt << m_bar << " ";

                line1 << m_bar << b_turt1 << m_bar << " ";
                line2 << m_bar << t_turt1 << m_bar << " ";

                line1 << m_bar << "\u22C3" << m_bar << " ";
                line2 << m_bar << "\u22c2" << m_bar << " ";

                line1 << m_bar << "\u2304" << m_bar << " ";
                line2 << m_bar << "\u2303" << m_bar << " ";

                line1 << m_bar << "\u2304" << m_bar << " ";
                line2 << m_bar << "\u2303" << m_bar << " ";

                line1 << m_bar << m_bar << m_bar << " ";
                line2 << m_bar << m_bar << m_bar << " ";


                std::cout << line1.str() << std::endl;
                std::cout << line2.str() << std::endl;


                break;
            }
        case 11:
            {


                std::vector<double> params = olistd::random_vector<double>(1);
                std::function<double(const std::vector<double> &)> func = 
                    [](const std::vector<double> & v){
                        double val = 0.0;
                        for(double value : v)
                            val += value * value;
                        return val;};

                std::cout << "params ";
                olistd::print(params);

                {
                    olistd::Gradient_descent gd;
                    std::vector<double> final = gd.solve(params, func);
                    olistd::print(final);
                }

                {
                    olistd::Adam adam;
                    std::vector<double> final = adam.solve(params, func);
                    olistd::print(final);
                }

                break;
            }
        case 12:
            {

                olistd::Graph graph(16);
                olistd::print(graph.data);
                olistd::print_matrix(graph.data);

                olistd::Graph_draw gd{graph};
                gd.draw();



                break;
            }
        case 13:
            {
                olistd::Thread threads{8};
                threads.add([]{std::cout << "test" << std::endl;});
                threads << []{std::cout << "test" << std::endl;};

                // threads.run();
                // threads.join();
                std::cout << "wait " << std::endl;

                const size_t max = 16;

                for(size_t i = 0; i < max; i++)
                    threads << [=]
                    {
                        std::cout << "test" + std::to_string(i) << std::endl;
                        sleep(1);
                    };

                // optional
                // threads.run();
                // OR
                // threads.join();
                break;
            }
        case 14:
            {
                auto my_func = [](const std::vector<double> & params)
                {
                    sleep(1);
                    return olistd::sq(params[0]) - 3.0 * params[1] + 5;;
                };

                olistd::working::Cached_function func{my_func};
                // deriv.use_cache = false;
                size_t max = 1e5;

                std::vector<double> answers(max);
                olistd::Timer time;
                for(size_t i = 0; i < max; i++)
                {
                    answers[i] = func( {0.0, 0.0} );
                }
                std::cout << answers[0] << std::endl;
                time.stop();

                break;
            }
        case 15:
            {
                // auto my_func = [](const std:double x, double y)
                // {
                //     // sleep(1);
                //     return olistd::sq(x) - 3.0 * y + 5;
                // };

                // olistd::Cached_function func{my_func, 0.0, 0.0};
                // // deriv.use_cache = false;
                // size_t max = 1;

                // std::vector<double> answers(max);
                // olistd::Timer time;
                // for(size_t i = 0; i < max; i++)
                // {
                //     answers[i] = func( 0.0, 0.0 );
                // }
                // std::cout << answers[0] << std::endl;
                // time.stop();
                auto my_func = [](const std::vector<double> & params)
                {
                    sleep(1);
                    return olistd::sq(params[0]) - 2.0 * params[1] + 5;;
                };


                // auto f = my_func;

                // olistd::working::Cached_function cached_f{my_func};
                // auto f = [&](const std::vector<double> & args)
                // {
                //     std::cout << "args ";
                //     for(auto & a : args)
                //         std::cout << a << ", ";
                //     std::cout << "\n";
                //     return cached_f.eval_verbose(args);
                //     // return cached_f(args);
                // };

                olistd::working::Cached_function cached_f{my_func};
                auto f = [&](const std::vector<double> & args)
                {
                    return cached_f(args);
                };

                std::vector<double> x = {0.0, 0.0}; 
                std::vector<double> gradients;
                // for each direction 
                for(int k = 0; k < 2; k++)
                    for(int i = 0; i < (int)x.size(); i++)
                    {
                        // for derivatives need the unit vector 
                        std::vector<int> derivs(x.size(), 0);
                        derivs[i] = 2;
                        std::vector<int> index;
                        for(size_t j = 0; j < x.size(); j++)
                            index.push_back(j);

                        double diff = olistd::partial_derivative_multi(f, x, derivs,index)(x);
                        gradients.push_back(diff);
                    }

                olistd::print(gradients);

                std::cout << "keys ";
                olistd::print(cached_f.get_cache().get_keys());
                std::cout << "vals ";
                olistd::print(cached_f.get_cache().get_vals());


                break;
            }
        case 16:
            {
                size_t counter = 0;
                auto my_func = [&](const std::vector<double> & params)
                {
                    // sleep(1);
                    counter++;
                    return olistd::sq(params[0]) + 2.0 * olistd::sq(params[1]) + 5;;
                };

                olistd::working::Cached_function cached_f{my_func};
                auto f = [&](const std::vector<double> & args)
                {
                    return cached_f(args);
                };

                std::vector<double> params = {-4, 2};

                double delta = 0.1;
                double eps = 1e-6;
                size_t max_iter = 1e2;

                std::vector<double> vals;

                bool gd = true;

                if(gd)
                {
                    olistd::Gradient_descent gd{delta, eps, max_iter};
                    // std::vector<double> vals = gd.solve(params, my_func);
                    std::vector<double> vals = gd.solve(params, f);
                }
                else 
                {
                    olistd::Adam adam{delta, eps, max_iter};
                    // std::vector<double> vals = adam.solve(params, my_func);
                    std::vector<double> vals = adam.solve(params, f);
                }


                std::cout << counter << " function evaluations " << std::endl;
                olistd::print(vals);
                std::cout << "-------------------------------------------------------------\n";
                std::cout << "Size of cache " << cached_f.get_cache().size() << std::endl;
                std::cout << "-------------------------------------------------------------\n";

                std::cout << "keys ";
                olistd::print(cached_f.get_cache().get_keys());
                std::cout << "vals ";
                olistd::print(cached_f.get_cache().get_vals());


                break;
            }
    }

    return 0;
}

