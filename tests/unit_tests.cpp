/**
 * @author      : oli (oli@oli-desktop)
 * @file        : unit_tests
 * @created     : Saturday Nov 19, 2022 18:44:27 GMT
 */

#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN
#include "doctest.h"

#include <olistd/olistdlib.hpp>
#include <olistd/threads>

TEST_CASE("VERSION NUMBER")
{
    std::cout << "OLISTD VERSION MAJOR NUMBER " << OLISTD_VERSION_MAJOR << std::endl;
    std::cout << "OLISTD VERSION MINOR NUMBER " << OLISTD_VERSION_MINOR << std::endl;
}

TEST_CASE("Combinatorics")
{
    SUBCASE("click_patterns")
    {
        const int m = 3;
        const int base = 2;
        std::vector<std::vector<int>> clicks = olistd::click_patterns(m, base);

        // olistd::print(clicks);
        REQUIRE(clicks[0] == std::vector<int>{0, 0, 0});
        REQUIRE(clicks[7] == std::vector<int>{1, 1, 1});

    }
    SUBCASE("PNR SUB SPACE")
    {
        std::vector<std::vector<int>> clicks = olistd::gen_pnr_click_patterns_k_subspace(3, 2, 2);
        // olistd::print(clicks);
        REQUIRE(clicks[0] == std::vector<int>{0, 0, 2});
        REQUIRE(clicks[1] == std::vector<int>{0, 1, 1});
    }
    SUBCASE("gen_threshold_click_patterns")
    {
        const int m = 3;
        std::vector<std::vector<int>> clicks = olistd::gen_threshold_click_patterns(m);

        // olistd::print(clicks);
        REQUIRE(clicks[0] == std::vector<int>{0, 0, 0});
        REQUIRE(clicks[7] == std::vector<int>{1, 1, 1});
    }
    SUBCASE("k_click_patterns")
    {
        const int m = 3;
        const int n = 2;
        std::vector<std::vector<int>> clicks = olistd::k_click_patterns(m, n);

        // olistd::print(clicks);
        REQUIRE(clicks[0] == std::vector<int>{1, 1, 0});
        REQUIRE(clicks[1] == std::vector<int>{1, 0, 1});
        REQUIRE(clicks[2] == std::vector<int>{0, 1, 1});
    }
    SUBCASE("gen_click_patterns")
    {
        const int m = 3;
        const int n = 2;
        const int min = 1;
        std::vector<std::vector<int>> clicks = olistd::gen_click_patterns(m, n, min);

        // olistd::print(clicks);
        REQUIRE(clicks[0] == std::vector<int>{1, 0, 0});
        REQUIRE(clicks[1] == std::vector<int>{0, 1, 0});
        REQUIRE(clicks[2] == std::vector<int>{0, 0, 1});
    }
}

TEST_CASE("DERIVATIVES")
{



}

TEST_CASE("FLAT_MAP")
{



}

TEST_CASE("THREADS")
{
    auto f = []()
    {
        // std::cout << "f" << std::endl;
        // sleep(1);
    };

    olistd::Thread threads{59};

    // olistd::Timer time;
    for(size_t i = 0; i < 100; i++)
    {
        threads << f;
    }
    // time.stop();

}

TEST_CASE("Display")
{

    olistd::display disp;
    
    std::cout << disp.rows << ", " << disp.cols << std::endl;

    std::vector<char> cs = olistd::lin_vec<char>(33,127);

    for(size_t i = 0; i < 4; i++)
        for(char c : cs)
            disp << c;

    disp.print();


}
