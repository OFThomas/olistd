# Derivatives

Used by:
```
#include <olistd/derivatives>
```

The derivatives file uses **Finite difference** methods to
calculate derivatives of C++ functions without analytic derivatives.
We do this by evaluating the function at many points and then adding
and subtracting the values to form a _stencil_ of points around the
value we wish to calculate the gradients of the function at.

This method is nice because it will provide an approximation to the
derivative of nearly any well-behaved function, easily without
having to worry about the analytic expression for the $n-th$ order
derivative. However there may be issues with numerical stability
(especially for higher order derivatives, which we approximate here
by repeatedly interpolating between lower order derivatives (using
finite difference method themselves recursively).The other tradeoff
is that evaluating many derivatives does not scale favourably with
this method, i.e. **we have to evaluate the function `f` many
times which can be slow if `f` is expensive to evaluate**.

#### First derivative of a scalar function
```c
double olistd::df(const std::function<double(double)> & f,
        double vals, int index = 0);
```
returns the first derivative of the function `f` evaluated at
`vals`. I.e. 
    $\frac{d}{dx}f(x) |_{x=vals}\,.$

For example with Lambdas:
```c
// y = f(x) = x^2
auto f = [](double x)
{
    return x * x;
};
double grad = olistd::df(f, 1.0);
// df/dx = 2x, evaluated at x = 1.0 
grad == 2.0;
```

Sometimes it can be confusing using the `auto` keyword so you can explicity
specify the type of the lambda using `std::function`:
```c
// y = f(x) = x^2
std::function<double(double)> f = [](double x)
{
    return x * x;
};
double grad = olistd::df(f, 1.0);
// df/dx = 2x, evaluated at x = 1.0 
grad == 2.0;
```

Or an already existing function:
```c
double f(double x)
{
    return x * x;
}

int main(void)
{
    double grad = olistd::df(f, 1.0);
    // df/dx = 2x, evaluated at x = 1.0 
    grad == 2.0;
    return 0;
}
```

## Any derivative of a scalar function

```c
std::function<double(double)> olistd::derivative(
        const std::function<double(double)> & F, 
        double val, int deriv, int index = 0);
```
The function takes a function `f` which has signature `double(double)` meaning
it takes a single double as an argument and returns a double, `val` is the point
at which the derivative is evaluated at and `deriv` is the $n-$th derivative,
`deriv = 1` means the first derivative, `deriv = 2` is the second derivative and
so on.

Usage is similar to `olistd::df` above, however it's important to note that
`olistd::derivative` actually returns a `std::function` object so to evaluate
the gradient you'll need to use the call operator `()` with the argument of the
point you wish to actually evaluate the gradient at `val`.

```c
auto f = [](double x)
{
    return sin(x);
};

double value = 3.141593; // M_PI

// usage 1, use auto as the return type 
auto d0 = olistd::derivative(f, value, 0);
double grad0 = d0(value); // sin(PI) == 0

// usage 2, specify the full return type
std::function<double(double)> d1 = olistd::derivative(f, value, 1);
double grad1 = d1(value); // cos(PI) == -1

// usage 3, call with value immediately to evaluate 
double grad2 = olistd::derivative(f, value, 2)(value); // -sin(PI) == 0
```

There's also a partial_derivative function 
```c
std::function<double(double)> olistd::partial_derivative(
        const std::function<double(double)> & F,
        double vals,
        const std::vector<int> & derivatives,
        const std::vector<int> & indices);
```

## Paritial_derivatives of multivariate functions
In general we probably are considering slighty more complex functions, rather
than just single parameter functions as above.

The base function is `olistd::df_multi`: 
```c
double olistd::df_multi(const std::function<double(const std::vector<double> &)> & f, 
        std::vector<double> vals, int index, double h);
```
The arguments is a single funtion object `f` which has the following signature,
`f` returns a `double` and takes a `std::vector<double>` as its arguments. As an
example, 
```c
std::function<double(const std::vector(double) &)> my_f = [](const
std::vector<double> & params)
{
    double sum = 0.0;
    for(size_t i = 0; i < params.size(); i++)
    {
        sum += std::pow(params[i], i);
    } 
    return sum;
}
```
`my_f` returns the sum of the arguments to the power of $i$,
```c
my_f({1.0, 2.0, 3.0}); // == 32
// as
// 0.0 + 1^1 + 2^2 + 3^3 = 1 + 4 + 27 = 32;
```
Note, we could also use `auto` for the lambda function return type.

### Multivalitate derivatives 

Our wrapper function,
```c
std::function<double(const std::vector<double> &)> olistd::derivative_multi(
        const std::function<double(const std::vector<double> &)> & F,
        const std::vector<double> & val,
        int deriv, 
        int index,
        double tol);
```

And for partial derivatives:
```c
std::function<double(const std::vector<double> &)> olistd::partial_derivative_multi(
        const std::function<double(const std::vector<double> &)> & F,
        const std::vector<double> & vals,
        const std::vector<int> & derivatives,
        const std::vector<int> & indices,
        double tol = EPSILON_MACHINE_PRECISION);
```
As an example, 
```c
std::function<double(const std::vector(double) &)> my_f = [](const
std::vector<double> & params)
{
    double sum = 0.0;
    for(size_t i = 0; i < params.size(); i++)
    {
        sum += std::pow(params[i], i);
    } 
    return sum;
}

const std::vector<double> points{11.0, 7.0, -9.0);
const std::vector<int> derivatives{1, 4, 5};
const std::vector<int> indices{0, 1, 2};

std::function<double(const std::vector<double> &)> derivative =
olistd::paritial_derivative_multi( my_f, points, derivatives, indices);

double gradient = derivative(points);
```


