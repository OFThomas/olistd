# Flat_map

Used by:
```
#include <olistd/flat_map>
```
This file consists of a single class, the `olistd::Flat_map` class. The flat_map
is an associative contianer that is implemented using two `std::vectors` to
store the keys and values. 

```c
template <typename Key, typename Val>
olistd::Flat_map<Key, Val> mymap; // types 

// Inserting 
bool Flat_map::insert(const Key & key, const Val & val);
Val & Flat_map::at(const Key & key);
const Val & Flat_map::at(const Key & key) const;

// lookup
Val & Flat_map::operator[](const Key & key);
std::pair<bool, Val &> Flat_map::find(const Key & key);
std::pair<bool, const Val &> Flat_map::find(const Key & key) const;

// Removing
bool Flat_map::remove(const Key & key);
void Flat_map::clear();

// info
size_t Flat_map::size();
bool Flat_map::empty();
const std::vector<Key> & Flat_map::get_keys() const;
const std::vector<Val> & Flat_map::get_vals() const;
```

Usage is similar to a `std::map`:
```c
olistd::Flat_map<int, int> map;

// to insert elements 
map.insert(0, 1); // key 0, val 1

const int & v = map[0]; // returns const ref to the val (1 in this case)
map[0]++; // increments the val (1 in this case) to 2
```


