# IO

Used by:
```
#include <olistd/io>
```

This header is split into two main groups, printing container to stdout and
writing containers to files. 

**`std::vector`, `std::map`, `std::pair` are nearly all supported for
`olistd::print(container)` or `olistd::write_file(container, filename)`**

## Printing functions 

The contianer writing to std out is based on [this](https://stackoverflow.com/questions/31130243/pretty-printing-nested-vectors) stackoverflow thread.

### `std::map`
The function prints `key; value` pairs for a `std::map` with a newline between
each (by default)
```c
template <typename T, typename U>
void olistd::print(const std::map<T, U> & map, bool newline = true);
``` 
Incase you only want to print the values of the `std::map`s in a
`std::vector<std::map>>`:
```c
template <typename T, typename U>
void print(const std::vector<std::map<T, U>> & v_map);
```

### `std::pair`
There is a simillar overload for `std::pair`
```c
template <typename T, typename U>
void olistd::print(const std::pair<T, U> & pair, bool newline = true);
```

### `std::vector`
There's a print overload for `std::vector`s and nested,
`std::vector<std::vector>>`
```c
template <class T>
void olistd::print(const std::vector<T> & data, bool newline = true);
```

There's a print for matrices using a flat `std::vector` **ROW MAJOR STORAGE
ORDER**
```c
template <class T>
void olistd::print_matrix(const std::vector<T> & data, size_t stride);
```
and an overload for square matrices where you don't need to specify the `stride`
```c
template <class T>
void olistd::print_matrix(const std::vector<T> & data);
```

There's a print for two vectors i.e. $x$ and $y$ columns of data, prints them
comma separated.
```c
template <typename X, typename Y>
void olistd::print(const std::vector<X> & x, const std::vector<Y> & y); 
```

### Printing of optical circuits 

Yep.
```c
void olistd::print_circuit(const std::vector<std::vector<int>> & v);

// the outer idx is the column and the inner is which rows to draw
// 2, 3,
// 2, 3,
// 1, 2,
// 2, 3,
// 0, 1,
// 1, 2,
// 2, 3,

// would look like
// 0 ---------u----
// 1 -----u---u-u--
// 2 -u-u-u-u---u-u
// 3 -u-u---u-----u
```

And
```c
void olistd::print_circuit_compressed(const std::vector<std::vector<int>> & v);

//0 ---b-----e-----------k-------
//1 ---b-c---e-----h-----k-l-----
//2 -----c-----f---h-i-----l-m---
//3 -a-----d---f-g---i-j-----m-n-
//4 -a-----d-----g-----j-------n-

// would look like
//0 -b---e---k-------
//1 -b-c-e-h-k-l-----
//2 ---c-f-h-i-l-m---
//3 -a-d-f-g-i-j-m-n-
//4 -a-d---g---j---n-
```



## Write files 

```c
template <typename X, typename T, typename U>
void olistd::write_file(const std::vector<X> & x, 
        const std::vector<std::map<T, U>> & v_map, 
        const std::string & filename = "");
```
Writes a `std::vector` and a `std::vector<std::map>` to a file, the default
filename is the empty string which will print to `stdout` instead of disk.
The function writes vector of x values and a new column for each `value` in the
map, ignores the map keys.

### `std::vector`

To write a single `std::vector` to disk (comma separated) which makes a single
row with multiple columns 
```c
template <class T>
void olistd::write_file(const std::vector<T> & data, 
        const std::string & filename);
```

To write a single `std::vector` to disk where each element is `newline`
separated, which makes a single column file with multiple rows.
```c
template <class T>
void olistd::write_file_transposed(const std::vector<T> & data, 
        const std::string & filename);
```

To write a nested `std::vector<std::vector<>>` to disk, each outer vector
corresponds to a different column and the inner vector are the rows of that
column, this funtion allows you to write different sized inner vectors to disk
and it will pad the missing elements with `tab` "\t" in the file so that the
other columns still approximately line up with each other, (comma separated).
```c 
template <class T>
void olistd::write_file(const std::vector<std::vector<T>> & data, 
        const std::string & filename);
```

There is a transposed version which writes a nested `std::vector<std::vector<>>`
as a function a of variable,
```c
template <class T>
void olistd::write_file_transposed(const std::vector<double> & variable,
        const std::vector<std::vector<T>> & data, 
        const std::string & filename);
```
in this function all the inner `std::vectors` have to be the same size.

To write two `std::vector`s as $x, y$ `tab` "\t" separated columns, 
```c
template <typename X, typename Y>
void olistd::write_file(const std::vector<X> & x,
        const std::vector<Y> & y,
        const std::string & filename);
```
Three `std::vector`s as $x, y, z$ `tab` "\t" separated columns,
```c
template <typename X, typename Y, typename Z>
void olistd::write_file(const std::vector<X> & x,
        const std::vector<Y> & y,
        const std::vector<Z> & z,
        const std::string & filename);
```

### `std::map`
To write a `std::map` to disk 
```c
template <typename X, typename T>
void olistd::write_file(const std::map<X, T> & map,
        const std::string & filename);
```

### `olistd::Flat_map`
```c
template <typename X, typename T>
void olistd::write_file(const olistd::Flat_map<X, T> & map,
        const std::string & filename);
```

### Printing for plots 

```c
template <class T>
T olistd::print_plot(const std::vector<T> & data, 
        const std::string & filename);
```

## Strip data

```c
// takes a file, loads it in
// strips for lines which are less than the best so far
// and we write those lines out to a new reduced file 
// const std::string filename = argv[2];
void olistd::strip_data(const std::string & filename);
```

## Reading csv

```c
template <typename T>
std::vector<std::vector<T>> olistd::read_csv(const std::string & filename, 
        int line_skip = 1);
```


